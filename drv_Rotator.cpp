/**********************************************************************************
 **********************************************************************************
 *
 *        @file   drv_Rotator.cpp
 *		  @author Duclos Timothé
 *
 *        @brief  
 */
 
 
/********************************************************************************** 
 *  Includes
 */
#include "drv_Rotator.h"

 
/********************************************************************************** 
 *  Private define
 */
#define ROTATOR_PIN		41
 
/********************************************************************************** 
 *  Exported functions
 */
void Rotator_init(void)
{
	pinMode(ROTATOR_PIN, OUTPUT);
}


void Rotator_Ungrip(void)
{
	digitalWrite(ROTATOR_PIN, LOW);
}



void Rotator_Grip(void)
{
	digitalWrite(ROTATOR_PIN, HIGH);
}





/************************************ END OF FILE ********************************/
