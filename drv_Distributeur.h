/**********************************************************************************
 **********************************************************************************
 *
 *        @file   drv_Distributeur.h
 *		  @author FICET Kevin
 *
 *        @brief  Pilotage stepper tourniquet
 */
 
#ifndef _DRV_DISTRIBUTEUR_H_
#define _DRV_DISTRIBUTEUR_H_

/********************************************************************************** 
 *  Includes
 */
#include <Arduino.h>



/**********************************************************************************
*  Exported Functions
*/
void Distrib_init(void);

void Distrib_Feed(void);
void Distrib_Replacement(void);



#endif /* _DRV_DISTRIBUTEUR_H_ */
 
/************************************ END OF FILE ********************************/
