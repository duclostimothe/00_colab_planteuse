/**********************************************************************************
 **********************************************************************************
 *
 *        @file   drv_UART.cpp
 *		  @author Duclos Timothé
 *
 *        @brief  
 */
 
 
/********************************************************************************** 
 *  Includes
 */
#include "drv_UART.h"
#include "util_FiFo.h"



/********************************************************************************** 
 *  Private variables
 */
static FiFo_s     UART_Rx            = {0};
static UartData_s UART_Rx_Buffer[10] = {0};



/********************************************************************************** 
 *  Exported functions
 */
 /**
  *		@brief		Initialisation de la FIFO et de l'UART
  */
void drvUart_init(void)
{
	
    Serial.begin(115200);
	  FiFo_init(&UART_Rx, 10, sizeof(UartData_s), (uint8_t*)&UART_Rx_Buffer);
}


void drvUart_Test(void)
{
  
  static UartData_s   BufferTemp  = {0};

  BufferTemp.IndexCmd = UART_CMD_Feed;
  BufferTemp.IndexStepper = 0x0;
  BufferTemp.nbIteration = 0x0;
  
  FiFo_Push2(&UART_Rx, &BufferTemp);


  
}


#define CONF_PRINT_RECEP    0

/**
 *		@brief		Reception de la data selon UartData_s
 */
void drvUart_main(void)
{
	static uint32_t     Timeout_ms  = 0;
	static UartData_s   BufferTemp  = {0};

	// Data dispo
	if(Serial.available() != 0)
	{
		static uint8_t Etape = 0;
		uint8_t Data = Serial.read();

    //Serial.print(Data);

		// Timeout
		if(Etape != 0)
		{
			if(millis() - Timeout_ms >= 250)
			{
				Etape                   = 0;
				BufferTemp.nbIteration  = 0;
				BufferTemp.IndexStepper = 0;
        BufferTemp.IndexCmd     = 0;
			}
			else if(Timeout_ms > millis())
			{
				Timeout_ms = millis();
			}
		}

    
  
		// recep
		switch(Etape)
		{
			// Premier octet recu
			case 0:
        BufferTemp.IndexCmd = Data;
        Timeout_ms = millis();
        Etape++;
				break;
			
			
			// Deuxieme octet recu
			case 1:
				// Index de commande doit être inférieur à 6
				if(Data < 6)
				{
				  BufferTemp.IndexStepper = Data;
				  Timeout_ms = millis();
				  Etape++;

          #if CONF_PRINT_RECEP == 1
            Serial.println("Debut recep");
          #endif /* CONF_PRINT_RECEP */
				}
       else
       {
            Etape                   = 0;
            BufferTemp.nbIteration  = 0;
            BufferTemp.IndexStepper = 0;
            BufferTemp.IndexCmd     = 0;
        
          #if CONF_PRINT_RECEP == 1
            Serial.println("KO Debut recep");
          #endif /* CONF_PRINT_RECEP */
       }
				break;

			// Premier octet poid fort de nbIteration (16 bits)
			case 2:
				BufferTemp.nbIteration = (uint16_t)(Data << 8) & 0xFF00;
				Etape++;
				break;

			// Deuxieme octet poid faible de nbIteration (16 bits)
			case 3:
				BufferTemp.nbIteration = BufferTemp.nbIteration | (uint16_t)(Data & 0x00FF);
				Etape++;
        #if CONF_PRINT_RECEP == 1
          Serial.print("Data conv : ");
          Serial.println(BufferTemp.nbIteration);
        #endif /* CONF_PRINT_RECEP */
				break;

			// Recept dernier octet fin de commande
			case 4:

				// validation fin de commande
				
				// RECEP OK
				if(Data == '\n')
				{
					FiFo_Push2(&UART_Rx, &BufferTemp);
          #if CONF_PRINT_RECEP == 1
            Serial.println("recep OK");
          #endif /* CONF_PRINT_RECEP */
				}
       else
       {
          #if CONF_PRINT_RECEP == 1
            Serial.println("recep KO");
          #endif /* CONF_PRINT_RECEP */
       }

				// RaZ
				Etape                   = 0;
				BufferTemp.nbIteration  = 0;
				BufferTemp.IndexStepper = 0;
        BufferTemp.IndexCmd     = 0;

				break;

			//---------------------------------------
			default:
				Etape                   = 0;
				BufferTemp.nbIteration  = 0;
				BufferTemp.IndexStepper     = 0;
        BufferTemp.IndexCmd     = 0;
				break;
		}
	}
}

/**
 *		@brief   Lectuer d'une commande
 *		@param[in]	*pDataOut		La ou écrire la donnée
 *		@retval		true  -> Data dispo
  *    				false -> Pas de Data 
 */
bool drvUart_Read(UartData_s *pDataOut)
{
	bool rtrn = false;
	
	if(FiFo_Pull(&UART_Rx, pDataOut) != FiFoState_Empty)
	{

		rtrn = true;
	}
	
	return rtrn;
}


static bool IsData = false;

void drvUART_TESTTT(void)
{
  IsData = true;
}

 static uint8_t i_toto = 0;
uint8_t drvUART_TESTTTREAD(void)
{
    static uint8_t Data1[6] = {
    0xAA,
    0x00,
    0x00,
    0x00,
    0xA
  };

 

  if(i_toto==5)
  {
    i_toto=0;
  }

  return Data1[i_toto++];
}

/**
 *    @brief    Reception de la data selon UartData_s
 */
void drvUart_mainTEST(void)
{
  static uint32_t     Timeout_ms  = 0;
  static UartData_s   BufferTemp  = {0};



  

  // Data dispo
  if(IsData == true)
  {
    static uint8_t Etape = 0;
    uint8_t Data = drvUART_TESTTTREAD();

    if(i_toto == 5)
    {
      IsData = false;
    }

    // Timeout
    if(Etape != 0)
    {
      if(millis() - Timeout_ms >= 250)
      {
        Etape                   = 0;
        BufferTemp.nbIteration  = 0;
        BufferTemp.IndexStepper = 0;
        BufferTemp.IndexCmd     = 0;
      }
      else if(Timeout_ms > millis())
      {
        Timeout_ms = millis();
      }
    }

    
  
    // recep
    switch(Etape)
    {
      // Premier octet recu
      case 0:
        BufferTemp.IndexCmd = Data;
        Timeout_ms = millis();
        Etape++;
        break;
      
      
      // Deuxieme octet recu
      case 1:
        // Index de commande doit être inférieur à 6
        if(Data < 6)
        {
          BufferTemp.IndexStepper = Data;
          Timeout_ms = millis();
          Etape++;

          #if CONF_PRINT_RECEP == 1
            Serial.println("Debut recep");
          #endif /* CONF_PRINT_RECEP */
        }
       else
       {
            Etape                   = 0;
            BufferTemp.nbIteration  = 0;
            BufferTemp.IndexStepper = 0;
            BufferTemp.IndexCmd     = 0;
        
          #if CONF_PRINT_RECEP == 1
            Serial.println("KO Debut recep");
          #endif /* CONF_PRINT_RECEP */
       }
        break;

      // Premier octet poid fort de nbIteration (16 bits)
      case 2:
        BufferTemp.nbIteration = (uint16_t)(Data << 8) & 0xFF00;
        Etape++;
        break;

      // Deuxieme octet poid faible de nbIteration (16 bits)
      case 3:
        BufferTemp.nbIteration = BufferTemp.nbIteration | (uint16_t)(Data & 0x00FF);
        Etape++;
        #if CONF_PRINT_RECEP == 1
          Serial.print("Data conv : ");
          Serial.println(BufferTemp.nbIteration);
        #endif /* CONF_PRINT_RECEP */
        break;

      // Recept dernier octet fin de commande
      case 4:

        // validation fin de commande
        
        // RECEP OK
        if(Data == '\n')
        {
          FiFo_Push2(&UART_Rx, &BufferTemp);
          #if CONF_PRINT_RECEP == 1
            Serial.println("recep OK");
          #endif /* CONF_PRINT_RECEP */
        }
       else
       {
          #if CONF_PRINT_RECEP == 1
            Serial.println("recep KO");
          #endif /* CONF_PRINT_RECEP */
       }

        // RaZ
        Etape                   = 0;
        BufferTemp.nbIteration  = 0;
        BufferTemp.IndexStepper = 0;
        BufferTemp.IndexCmd     = 0;

        break;

      //---------------------------------------
      default:
        Etape                   = 0;
        BufferTemp.nbIteration  = 0;
        BufferTemp.IndexStepper     = 0;
        BufferTemp.IndexCmd     = 0;
        break;
    }
  }
}

 
/************************************ END OF FILE ********************************/
