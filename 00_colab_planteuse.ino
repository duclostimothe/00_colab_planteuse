/**
 *    @brief      Planteuse oignon bulbe
 *    
 *    
 * 
 * 
 * 
 */





 
/********************************************************************************** 
 *  Includes
 */
#include "drv_Steppers.h"
#include "drv_UART.h"
#include "drv_Distributeur.h"
#include "drv_Rotator.h"




 
/********************************************************************************** 
 *  Exported functions
 */
void setup() 
{
  
  drvUart_init();
  Distrib_init();
  Steppers_init();
  Rotator_init();

  Serial.println("Coucou\n");
  //delay(1000);
  //drvUART_TESTTT();
  
/*
  Stepper_SetDirection(StepperDirection_1);
  delay(10);
  Stepper_Write(10);
  delay(100);*/
}





void loop() 
{
  static uint32_t MS_Timer = 0;
  
  UartData_s CMD = {0};
  
  drvUart_main();

 /* if(millis() - MS_Timer > 1000)
  {
    MS_Timer = millis();
    Serial.print("TEST\n");
  }*/

  if(drvUart_Read(&CMD) == true)
  {
      switch(CMD.IndexCmd)
      {
        case UART_CMD_init:
          Distrib_Replacement();
          Serial.print("Done\n");
          break;
        
        case UART_CMD_Feed:
          Distrib_Feed();
          Serial.print("Done\n");
          break;

        case UART_CMD_Step:
          Stepper_SetDirection(CMD.IndexStepper);
          delay(10);
          Stepper_Write(CMD.nbIteration);
          delay(100);
          Serial.print("Done\n");
          break;

        case UART_CMD_Ungrip:
          Rotator_Ungrip();
          Serial.print("Done\n");
          break;

        case UART_CMD_Grip:
          Rotator_Grip();
          Serial.print("Done\n");
          break;

        default:
          Serial.print("KO\n");
          break;
      }

  }


/*  if(millis() - MS_Timer >= 3000)
  {
    MS_Timer = millis();
    drvUART_TESTTT();
  }*/

  
}
