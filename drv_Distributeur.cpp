/**********************************************************************************
 **********************************************************************************
 *
 *        @file   drv_Distributeur.cpp
 *		  @author FICET Kevin
 *
 *        @brief  Pilotage stepper tourniquet
 */
 


/**********************************************************************************
    Includes
*/
#include "drv_Distributeur.h"
#include "util_FIFO.h"

/**********************************************************************************
   Defines
*/

#define PIN_STEPPER_DIR       2
#define PIN_STEPPER_PULSE     3

#define PIN_CAPTEUR_MAGNETIQUE	29
#define CapUltra  				33




/**********************************************************************************
 *   Private Functions
 */
 /**
  *		@brief		Pilotage blocant stepper
  */
static void Distrib_Write(uint16_t nb_Iteration_Distrib)
{
  uint16_t i_nbTour_Distrib = 0;

	for (i_nbTour_Distrib = 0; i_nbTour_Distrib < nb_Iteration_Distrib; i_nbTour_Distrib++);
	{
		  digitalWrite(PIN_STEPPER_PULSE, HIGH);
		  delayMicroseconds(200);
		  
		  digitalWrite(PIN_STEPPER_PULSE, LOW);
		  delayMicroseconds(500);
	}
  
}




/**********************************************************************************
    Exported Functions
*/

void Distrib_Replacement(void)
{
  // Tant que le capteur magnétique ne détecte rien, on fait tourner le moteur
  while(digitalRead(PIN_CAPTEUR_MAGNETIQUE) == 1)
  {
      Distrib_Write(1);
  }
}


/**
 *    @brief    Initialisation des pins utiles au pilotage du stepper
 */
void Distrib_init(void)
{

	pinMode(PIN_STEPPER_DIR,    OUTPUT);
	pinMode(PIN_STEPPER_PULSE,  OUTPUT);

	pinMode(PIN_CAPTEUR_MAGNETIQUE,  INPUT);
	//pinMode(CapUltra, INPUT);
}


/**
 *    @brief    Initialisation des pins utiles au pilotage du stepper
 */
void Distrib_Feed(void)
{
    static uint8_t nb_Passage = 0;

    if(nb_Passage < 3)
    {
        for(uint16_t i=0; i< 2000; i++)
        {
    		    Distrib_Write(28000);			//Nombre itération pour 90°
        }
    
        nb_Passage++;
    }
    else
    {
        Distrib_Replacement();
        nb_Passage = 0;
    }

}

/************************************ END OF FILE ********************************/
