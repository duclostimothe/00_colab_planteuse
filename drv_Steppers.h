/**********************************************************************************
 **********************************************************************************
 *
 *        @file   drv_Steppers.h
 *		  @author Duclos Timothé
 *
 *        @brief  
 */
 
#ifndef _DRV_STERPPERS_H_
#define _DRV_STERPPERS_H_
 
 
 
 
/********************************************************************************** 
 *  Includes
 */
#include <Arduino.h>


 
/********************************************************************************** 
 *  Exported types
 */
 /**
  *		@brief		Liste des mouvements possibles
  */
typedef enum {
  StepperDirection_1 = 0,
  StepperDirection_2,
  StepperDirection_3,
  
  StepperDirection_1_inv,
  StepperDirection_2_inv,
  StepperDirection_3_inv,

  nb_StepperDirection
  
}StepperDirection_e;





/**********************************************************************************
 *	Exported Functions
 */
void Steppers_init(void);

void Stepper_SetDirection	(StepperDirection_e Direction);
void Stepper_Write			(uint16_t nb_Iteration);
 
  
 
 
#endif /* _DRV_STERPPERS_H_ */
 
/************************************ END OF FILE ********************************/
