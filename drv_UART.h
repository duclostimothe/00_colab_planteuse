/**********************************************************************************
 **********************************************************************************
 *
 *        @file   drv_UART.cpp
 *		  @author Duclos Timothé
 *
 *        @brief  
 */
 
 
#ifndef _DRV_UART_H_
#define _DRV_UART_H_
 
 
/********************************************************************************** 
 *  Includes
 */
#include <Arduino.h>






/********************************************************************************** 
 *  Exported Types
 */
 typedef enum {
	 
	UART_CMD_Feed = 0XAA,
	UART_CMD_Step = 0xBB,
  UART_CMD_init = 0xCC,
  
  UART_CMD_Ungrip = 0xDC,
  UART_CMD_Grip = 0xDD,
  
	
 }UART_CMD_List_e;
 
 
 
/**
 *	@brief		Structure de la donnée
 */
typedef struct __attribute__ ((__packed__)) {

  uint8_t  IndexCmd;
  uint8_t  IndexStepper;		 // Index de la commande (de 0 à 5)
  uint16_t nbIteration;	        // de 1 à TBD
  
}UartData_s;




/********************************************************************************** 
 *  Exported functions
 */
void drvUart_init(void);
void drvUart_main(void);

bool drvUart_Read(UartData_s *pDataOut);
void drvUart_Test(void);

void drvUART_TESTTT(void);
void drvUart_mainTEST(void);

#endif /* _DRV_UART_H_ */
 
/************************************ END OF FILE ********************************/
