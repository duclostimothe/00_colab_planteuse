/**********************************************************************************
 **********************************************************************************
 *
 *        @file   drv_Rotator.h
 *		  @author Duclos Timothé
 *
 *        @brief  
 */
 
#ifndef _DRV_ROTATOR_H_
#define _DRV_ROTATOR_H_
 
/********************************************************************************** 
 *  Includes
 */
#include <Arduino.h>


 
/********************************************************************************** 
 *  Exported functions
 */
void Rotator_init(void);


void Rotator_Ungrip(void);
void Rotator_Grip(void);




#endif /* _DRV_ROTATOR_H_ */

/************************************ END OF FILE ********************************/