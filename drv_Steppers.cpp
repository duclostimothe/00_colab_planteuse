/**********************************************************************************
 **********************************************************************************
 *
 *        @file   drv_Steppers.cpp
 *		  @author Duclos Timothé
 *
 *        @brief  Pilotage de 3 steppers de manière synchrone et blocante
 */
 
 
/********************************************************************************** 
 *  Includes
 */
#include "drv_Steppers.h"
#include "util_FIFO.h"


 
/********************************************************************************** 
 *  Defines
 */
#define PIN_STEPPER_1_DIR       4
#define PIN_STEPPER_1_PULSE     5

#define PIN_STEPPER_2_DIR       6
#define PIN_STEPPER_2_PULSE     7

#define PIN_STEPPER_3_DIR       8
#define PIN_STEPPER_3_PULSE     9




/**********************************************************************************
 *	Exported Functions
 */
 /**
  *		@brief		Initialisation des pins utiles au pilotage des steppers
  */
void Steppers_init(void)
{
	pinMode(PIN_STEPPER_1_DIR,    OUTPUT);
	pinMode(PIN_STEPPER_1_PULSE,  OUTPUT);

	pinMode(PIN_STEPPER_2_DIR,    OUTPUT);
	pinMode(PIN_STEPPER_2_PULSE,  OUTPUT);

	pinMode(PIN_STEPPER_3_DIR,    OUTPUT);
	pinMode(PIN_STEPPER_3_PULSE,  OUTPUT);
}
 
/**
 * 		@brief   	Affectation des PIN DIR des steppers en fonction du type
 *					de mouvement demandé
 *		@param[in]	Direction		Direction demandé (StepperDirection_e)
 */
void Stepper_SetDirection(StepperDirection_e Direction)
{
	switch(Direction) 
	{
		case StepperDirection_1:
      digitalWrite(PIN_STEPPER_1_DIR, LOW);
      digitalWrite(PIN_STEPPER_2_DIR, HIGH);
      digitalWrite(PIN_STEPPER_3_DIR, HIGH);
			break;
	  
		case StepperDirection_2:
      digitalWrite(PIN_STEPPER_1_DIR, LOW);
      digitalWrite(PIN_STEPPER_2_DIR, HIGH);
      digitalWrite(PIN_STEPPER_3_DIR, LOW);
			break;
		  
		case StepperDirection_3:
      digitalWrite(PIN_STEPPER_1_DIR, LOW);
      digitalWrite(PIN_STEPPER_2_DIR, LOW);
      digitalWrite(PIN_STEPPER_3_DIR, HIGH);
			break;
		  
		case StepperDirection_1_inv:
      digitalWrite(PIN_STEPPER_1_DIR, HIGH);
      digitalWrite(PIN_STEPPER_2_DIR, LOW);
      digitalWrite(PIN_STEPPER_3_DIR, LOW);
			break;
		  
		case StepperDirection_2_inv:
      digitalWrite(PIN_STEPPER_1_DIR, HIGH);
      digitalWrite(PIN_STEPPER_2_DIR, LOW);
      digitalWrite(PIN_STEPPER_3_DIR, HIGH);
			break;
		  
		case StepperDirection_3_inv:
      digitalWrite(PIN_STEPPER_1_DIR, HIGH);
      digitalWrite(PIN_STEPPER_2_DIR, HIGH);
      digitalWrite(PIN_STEPPER_3_DIR, LOW);
			break;
	  
	default:
	  break;

  
  }
}  
  
  
/**
 * 		@brief   	Pilotage des steppers (dont la direction est réglé par Stepper_SetDirection)
 *					d'un certain nombre de steps de manière blocante
 *		@param[in]	nb_Iteration		Nombre d'itérations
 */
 void Stepper_Write(uint16_t nb_Iteration)
{
    uint16_t i_nbTour = 0;
    
    for(uint16_t i=0; i<nb_Iteration; i++)
    {
        for(i_nbTour=0; i_nbTour<50000; i_nbTour++);
        {
            digitalWrite(PIN_STEPPER_1_PULSE, HIGH);
            digitalWrite(PIN_STEPPER_2_PULSE, HIGH);
            digitalWrite(PIN_STEPPER_3_PULSE, HIGH);
            delayMicroseconds(400);
            digitalWrite(PIN_STEPPER_1_PULSE, LOW);
            digitalWrite(PIN_STEPPER_2_PULSE, LOW);
            digitalWrite(PIN_STEPPER_3_PULSE, LOW);
            delayMicroseconds(1000);
        }
    }
}
 
 
 
 
 
 
/************************************ END OF FILE ********************************/
